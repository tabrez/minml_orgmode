'''Numpy basics'''

### https://jakevdp.github.io/PythonDataScienceHandbook/02.01-understanding-data-types.html
### https://docs.scipy.org/doc/numpy-1.15.0/user/quickstart.html

# ndarrays
# indexing and slicing
# indexing into 4 dimensional array; np.pad
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1)
x = np.random.randn(4, 3, 3, 2)
pad = 3
x_pad = np.pad(x, ((0, 0), (pad, pad), (pad, pad), (0, 0)),
               'constant',
               constant_values=0)

print("x.shape =", x.shape)
print("x_pad.shape =", x_pad.shape)

print("x = ", x)
print("x[0] = ", x[0])
print("x[0,1] =", x[0, 1])
print("x[0,1,0] =", x[0, 1, 0])
print("x[0,1,0,1] =", x[0, 1, 0, 1])

print("x_pad =", x_pad)

print("x[1,1] =", x[1, 1])
print("x_pad[0,0] =", x_pad[0, 2])

fig, axarr = plt.subplots(1, 2)
axarr[0].set_title('x')
axarr[0].imshow(x[0, :, :, 0])
axarr[1].set_title('x_pad')
axarr[1].imshow(x_pad[0, :, :, 0])
# vectorised functions and broadcasting
# matrix operations

### https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.dot.html

### log functions
yp = np.linspace(0.001, 0.999, 30)
logyp = np.log(yp)
_logyp = -np.log(yp)
log1_yp = np.log(1 - yp)
_log1_yp = -np.log(1 - yp)


def do_eeet():
    fig, axes = plt.subplots(2, 2)
    axes[0, 0].plot(yp, logyp)
    axes[0, 0].set_title('log yp')

    axes[0, 1].plot(yp, _logyp)
    axes[0, 1].set_title('-log yp')

    axes[1, 0].plot(yp, log1_yp)
    axes[1, 0].set_title('log(1-yp)')

    axes[1, 1].plot(yp, _log1_yp)
    axes[1, 1].set_title('-log(1-yp)')

    plt.show()


do_eeet()


def do_it():
    ax1 = plt.subplot(211)
    ax1.plot(yp, logyp)
    ax1.set_title('log yp / -log yp')

    # ax1 = plt.subplot(222, sharex=ax1, sharey=ax1)
    ax1.plot(yp, _logyp)
    # ax2.set_title('-log yp')

    ax3 = plt.subplot(212, sharex=ax1, sharey=ax1)
    ax3.plot(yp, log1_yp)
    ax3.set_title('log(1-yp) / -log(1-yp)')

    # ax4 = plt.subplot(224, sharex=ax1, sharey=ax1)
    ax3.plot(yp, _log1_yp)
    # ax4.set_title('-log(1-yp)')

    plt.xlim(-0.1, 1.1)
    plt.ylim(-8, 8)

    plt.show()


do_it()

# concat, stack
arr = [[1, 2, 3, 4], [4, 5, 6, 7], [8, 9, 10, 11]]
newrow = [[12, 13, 14, 15]]
np.concatenate(([['TV', 'Radio', 'Newspaper', 'Sales']], arr[:5, :], newrow),
               axis=0)

# how reshape(-1) works:
a = np.random.randn(4, 3, 2)
print(a)
a.reshape(3, 4, 2)
a.reshape(6, 4)
a.reshape(24)  # error
a.reshape(24, 1)
a.reshape(3, -1)
a.reshape(-1)

# np.ravel, np.flatten, np.squeeze

a = [[1, 11], [1, 2], [3, 3, 1]]
np.array(a)

a = [[1, 11], [1, 2], [3, 3]]
np.array(a)

[np.zeros((3, 2)) for i in range(1)]

# iterate in reverse direction
for i in range(5, 0, -1):
    print(i)
for i in reversed(range(5)):
    print(i)

# np.sum, axis, keepdims
np.sum(np.array([1, 2, 3]))
np.sum([1, 2, 3])
np.sum([[1, 2], [3, 4], [5, 6]])
np.sum([[1, 2], [3, 4], [5, 6]], axis=0)
np.sum([[1, 2], [3, 4], [5, 6]], axis=0).shape
np.sum([[1, 2], [3, 4], [5, 6]], axis=0, keepDims=True)
np.sum([[1, 2], [3, 4], [5, 6]], axis=1, keep_dims=True)

# newaxis
a = np.array([[1, 2, 3], [2, 1, 9], [8, 1, 3]])
a[:, 1]
a[:, 1, np.newaxis]
a[:, np.newaxis, 1]
a[np.newaxis, :, 1]
# exercise: try with 3 dimensional numpy array

# argmax
a = np.arange(6).reshape(2, 3)
a
# array([[0, 1, 2],
#        [3, 4, 5]])
np.argmax(a)
5
np.argmax(a, axis=0)
# array([1, 1, 1])
np.argmax(a, axis=1)
# array([2, 2])

# all elements in a 2d array are finite?
a = np.array([[1, 2, 3], [4, 5, 6]])
np.isfinite(a)
# not np.isfinite(a)
# ?np.all
np.isfinite(a).all()

# why does second row get converted to float?
arr = [[1.2, 3.1, 44.2], [1, 1, 1], [192.3, 12.54, 3.145]]
np.array(arr)

# reshape only rows or only columns or any (dim - 1) axes
arr = [[1, 2, 3], [4, 5, 6]]
np.array(arr).reshape(-1, 2)
np.array(arr).reshape(-1, 3)
np.array(arr).reshape(-1, 4)
np.array(arr).reshape(-1, 6)

# find the first of the pair in a collection of tuples whose second pair
# is the smallest
costs = [(2, 9), (3, 12), (5, -1), (8, 7)]
np.argmin(costs, axis=1)
np.argmin(costs, axis=0)

i = np.argmin(costs, axis=0)
costs[i[1]][0]
