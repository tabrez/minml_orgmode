# from pandas import read_csv, DataFrame
# import matplotlib.pyplot as plt
# import altair as alt
# import plotnine as pn


def read_data(path):
    res = []
    with open(path, 'r') as file:
        for line in file:
            res.append(line.strip('\n'))
    return res


def frequency_table(list):
    res = {}
    for word in list:
        if not (word in res):
            res[word] = 1
        else:
            res[word] += 1

    return res


data = read_data('histogram.csv')
# print(data)

table = frequency_table(data)
# print(table)

# plt.bar(table.keys(), table.values())
# plt.hist(data)
# plt.show()

# chart = alt.Chart(DataFrame(data, columns=['colours'])).mark_bar().encode(
#     alt.X('colours'),
#     y='count()'
# )
# chart.serve()
# chart.save('colours.html')

# Continous variable

response_times = [568,
                  577,
                  581,
                  640,
                  641,
                  645,
                  657,
                  673,
                  696,
                  703,
                  720,
                  728,
                  729,
                  777,
                  808,
                  824,
                  825,
                  865,
                  875,
                  1007]

# p = (pn.ggplot(DataFrame(response_times, columns=['res_time']),
#                pn.aes(x='res_time'))
#      + pn.geom_histogram(binwidth=100))
# print(p)
