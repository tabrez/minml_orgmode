
from linreg import *
def load_dataset(path, cols):
    df = read_csv(path, dtype=np.float32, usecols=cols, na_values='?')
    return df.values

d = load_dataset('data/auto-mpg.csv', cols=[0,1,2,3,4,5])
#print(d.isnull().sum())
d = d[~np.isnan(d).any(axis=1)]
#print('d: ', d.shape, d[:5,:])

# move first(target) column to last position, drop 4th
df = d[:, [1, 2, 3, 4, 5, 0]]
#print(df.shape, df[:5,:])

X, X_val, y, y_val, W, b = prep_data(df)
#print(X.shape, y.shape, W.shape, b)
#compute_cost(X, y, W, b)

print(sklearn_ols_r_2(X, y, X_val, y_val))

print(sklearn_ridge_r_2(X, y, X_val, y_val))

Wf, bf, C = gradient_descent(X, y, W, b, 350000, 0.0000002, False)
compare_r_2(X, y, X_val, y_val, Wf, bf)
