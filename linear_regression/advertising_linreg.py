
#from linreg import *


def load_dataset(path, cols):
    df = read_csv(path, dtype=np.float32, usecols=cols, na_values='?')
    #print((df.shape, df.head()))
    #print(d.values[:, 1:])
    return df.values

def try_toy_dataset():
    X = np.array([[1,2],[3,4],[5,7]])
    y = np.array([[1, 0]])
    W = np.array([[1, 1, 1]])
    b = 1
    return (X, y, W, b)
compute_cost(*try_toy_dataset())

# The error can be anything as we're using random values for W and b

df = load_dataset('data/advertising.csv', cols=[1,2,3,4])
X, X_val, y, y_val, W, b = prep_data(df, 0.7)
compute_cost(X, y, W, b)

#X, X_val, y, y_val, W, b = prep_data(df)

Wf, bf, C = gradient_descent(X, y, W, b, 2000, 0.00001, True)
#compute_cost(X, y, Wf, bf)
C

def plot_cost_vs_iters(C):
    ci_nda = np.array(C)
    df = DataFrame(np.stack((ci_nda[:,0], ci_nda[:,1]), axis=1),
                   columns=['n_iters', 'cost'])
    plot = (pn.ggplot(df, pn.aes('n_iters', 'cost'))
            + pn.geom_point()
            + pn.geom_line())
    return plot
p = plot_cost_vs_iters(C)
p.save('images/cost_vs_iters.png', width=15, height=8)
# print(plot_cost_vs_iters(C))

def cost_vs_learning_rates(X, y, W, b, alphas, iters=900):
    costs = []
    for alpha in alphas:
        Wg, bg, Cg = gradient_descent(X, y, W, b, iters, alpha)
        calpha = compute_cost(X, y, Wg, bg)
        # print((alpha, calpha))
        costs.append([alpha, calpha])
    return costs

alphas = [0.000001, 0.000005, 0.000009, 0.00001, 0.00002, 0.00003, 0.0000657]
ca_nda = np.array(cost_vs_learning_rates(X, y, W, b, alphas))
ca_nda

def plot_cost_vs_learning_rate(C):
    ca_nda = np.array(cost_vs_learning_rates(X, y, W, b, alphas))
    df = DataFrame(np.stack((ca_nda[:,0], ca_nda[:,1]), axis=1),
                   columns=['alpha', 'cost'])
    plot = (pn.ggplot(df, pn.aes('alpha', 'cost'))
            + pn.geom_point()
            + pn.geom_line())
    return plot

p = plot_cost_vs_learning_rate(C)
p.save('images/cost_vs_alpha.png', width=15, height=8)

# train_error = error(X, y, Wf, bf)
# val_error = error(X_val, y_val, Wf, bf)
# [('Train error:', train_error), ('Validation error:', val_error)]

print(compare_r_2(X, y, X_val, y_val, Wf, bf))

sklearn_ols_r_2(X, y, X_val, y_val)

sklearn_ridge_r_2(X, y, X_val, y_val)
