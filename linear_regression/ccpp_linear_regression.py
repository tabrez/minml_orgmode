
from linreg import *
from pandas import concat
import glob

#df = read_csv('data/ccpp_sheet1.csv')

### import os for making this platform independent
files = glob.glob('data/ccpp*.csv')
df = concat((read_csv(f) for f in files)).values
#print(a.isnull().sum())
#print('df: ', df.shape, df[:5])

X, X_val, y, y_val, W, b = prep_data(df, 0.95)
#print(X.shape, y.shape, X_val.shape, y_val.shape, W.shape, b)
compute_cost(X, y, W, b)

sklearn_ols_r_2(X, y, X_val, y_val)

sklearn_ridge_r_2(X, y, X_val, y_val)

#from sklearn.metrics import r2_score
#print(r2_score(y.ravel(), compute_Z(X, Wf, bf).ravel()))
Wf, bf, C = gradient_descent(X, y, W, b, 19000, 0.00000193, False)
compare_r_2(X, y, X_val, y_val, Wf, bf)
