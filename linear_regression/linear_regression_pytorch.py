"""Linear Regression using PyTorch."""

import pandas as pd
import plotnine as pl
import torch


def gradient_descent(X, y, initial_theta, num_iters, alpha):
    """Use Gradient Descent method to find optimal theta parameter values.

    gradient = gradient - (alpha/m) * (X' . ((X . theta) - y) ^ 2)
    """
    theta = initial_theta
    m = X.size()[0]
    for i in range(num_iters):
        h_x = X @ theta
        diff = X.t() @ (h_x - y)
        theta = theta - (alpha / m) * diff
    return theta


def train_model(X, y):
    """Learn the parameters to fit the dataset."""
    n = X.size()[1]
    initial_theta = torch.zeros(n, 1)
    return gradient_descent(X, y, initial_theta, num_iters=1500, alpha=0.01)


def compute_cost(X, y, theta):
    """Compute the error based on theta values.

    cost = (1/2m) * sum(((X . theta) - y) ^ 2)
    """
    m = X.size()[0]
    h_x = X @ theta
    diff = h_x - y
    return (1/(2*m)) * torch.sum(diff ** 2)


def predict(X, theta):
    """Predict the result for new value of X based on theta."""
    return X @ theta


def main():
    """Prepare the data and set up calls  to various functions."""
    df = pd.read_csv('ex1data1.txt',
                     index_col=False,
                     header=None,
                     names=['X', 'y'])
    m = df.shape[0]
    input = torch.Tensor(df.iloc[:, 0].values).view(m, 1)
    row1 = torch.ones(m).view(m, 1)
    X = torch.cat([row1, input], dim=1)
    y = torch.Tensor(df.iloc[:, 1].values).view(m, 1)

    # p = (pl.ggplot(df, pl.aes('X', 'y'))
    #      + pl.geom_point())
    # print(p)
    print(f'Theta:[0,0]: {compute_cost(X, y, torch.Tensor([[0], [0]]))}')
    print(f'Theta:[-1,2]: {compute_cost(X, y, torch.Tensor([[-1], [2]]))}')

    theta = train_model(X, y)
    print(f'Theta: {theta}')
    p = (pl.ggplot(df, pl.aes('X', 'y'))
         + pl.geom_point()
         + pl.geom_abline(pl.aes(intercept=theta[0], slope=theta[1])))
    print(p)
    cost = compute_cost(X, y, theta)
    print(f'Cost: {cost}')

    new_x = torch.Tensor([1, 3.5])
    pval = predict(new_x, theta)
    print(f'Predicted value: {pval}')
    print(f'Predicted value: {predict(torch.Tensor([1, 10]), theta)}')


main()
