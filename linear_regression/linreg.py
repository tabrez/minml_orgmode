
import numpy as np
from pandas import read_csv, DataFrame
import plotnine as pn
from sklearn.linear_model import LinearRegression, Ridge

def init_all(df):
    X = df[:, :-1]
    y = df[:, -1]
    #print((X.shape, y.shape))
    X = np.transpose(X) # or use df[:. :-1].T above
    #X.flags.writeable = False
    n = X.shape[0]
    m = X.shape[1]
    y = y.reshape(1, m) # or use df[np.newaxis, :, -1] above
    #y.flags.writeable = False
    #print((X.shape, y.shape))
    # (X[:5], y[:5])
    np.random.seed(42)
    W = np.random.rand(1, n)
    b = np.random.rand()
    #print((n, m, b, W.shape))
    return (X, y, W, b)

def train_validate_split(X, y, pct):
    np.random.seed(42)
    # generate a random permutation of m numbers(0 to m-1)
    indices = np.random.permutation(X.shape[1])
    train_size = int(len(indices) * pct)
    train_indices = indices[:train_size]
    validate_indices = indices[train_size:]
    return (X[:, train_indices],
            X[:, validate_indices],
            y[:, train_indices],
            y[:, validate_indices])

def compute_Z(X, W, b):
    return (W @ X) + b


def compute_cost(X, y, W, b):
    Z = compute_Z(X, W, b)
    A = Z
    return np.average((A - y) ** 2)

def prep_data(df, pct):
    X, y, W, b = init_all(df)
    X, X_val, y, y_val = train_validate_split(X, y, pct)
    return (X, X_val, y, y_val, W, b)

def compute_gradients(X, A, y):
    m = y.shape[1]
    dJdw = (1. / m) * ((A - y) @ X.T)
    dJdb = np.average(A - y)
    return (dJdw, dJdb)

def gradient_descent(X, y, W, b, iters, alpha, store_costs=False):
    costs = []
    for i in range(iters+1):
        step = int(iters / 9)
        if store_costs and (i % step == 0):
            costs.append([i, compute_cost(X, y, W, b)])
        A = compute_Z(X, W, b)
        m = X.shape[1]
        (dw, db) = compute_gradients(X, A, y)
        W = W - (alpha * dw)
        b = b - (alpha * db)
    return (W, b, costs)

def predict(X, W, b):
    return compute_Z(X, W, b)


def error(X, y, W, b):
    return compute_cost(X, y, W, b)


def r_2(X, y, W, b):
    A = Z = compute_Z(X, W, b)
    sme = np.sum((np.mean(y) - y) ** 2)
    sfe = np.sum((A - y) ** 2)
    return 1. - (sfe / sme)


def compare_r_2(X, y, X_val, y_val, W, b):
    train_r_2 = r_2(X, y, W, b)
    val_r_2 = r_2(X_val, y_val, W, b)
    return [('Our train r_2:', train_r_2),
            ('Our validation r_2:', val_r_2)]

def sklearn_ols_r_2(X, y, X_val, y_val):
    lr = LinearRegression()
    lr = lr.fit(X.T, y.ravel())
    sk_train_r_2 = lr.score(X.T, y.ravel())
    sk_val_r_2 = lr.score(X_val.T, y_val.ravel())
    return [('sklearn ols train r_2:', sk_train_r_2),
            ('sklearn ols validation r_2:', sk_val_r_2)]

def sklearn_ridge_r_2(X, y, X_val, y_val):
    lr = Ridge(max_iter=1000, tol=1e-3, solver='sag')
    lr = lr.fit(X.T, y.ravel())
    sk_train_r_2 = lr.score(X.T, y.ravel())
    sk_val_r_2 = lr.score(X_val.T, y_val.ravel())
    # print(lr.get_params())
    return [('sklearn ridge train r_2:', sk_train_r_2),
            ('sklearn ridge validation r_2:', sk_val_r_2)]
