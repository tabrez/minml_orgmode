
import numpy as np
from pandas import read_csv, DataFrame
import plotnine as pn

def load_dataset(path):
    df = read_csv(
        path,
        header=None,
        dtype=np.float32,
        na_values='?',
        usecols=[1,2,3,4,5,7,8,9,10]
    )
    return df.values

def init_all(df):
    X = df[:, :-1]
    y = df[:, -1]
    # print((X.shape, y.shape))
    X = np.transpose(X)
    #X.flags.writeable = False
    n = X.shape[0]
    m = X.shape[1]
    y = y.reshape(1, m)
    #y.flags.writeable = False
    # print((X.shape, y.shape))
    # (X[:5], y[:5])
    np.random.seed(42)
    W = np.random.rand(1, n)
    b = np.random.rand()
    # print((n, m, b, W.shape))
    return (X, y, W, b)

# generate a random permutation of m numbers(0 to m-1)
def train_validate_split(X, y, pct):
    indices = np.random.permutation(X.shape[1])
    train_size = int(len(indices) * pct)
    train_indices = indices[:train_size]
    validate_indices = indices[train_size:]
    return (X[:, train_indices],
            X[:, validate_indices],
            y[:, train_indices],
            y[:, validate_indices])

def compute_Z(X, W, b):
    return (W @ X) + b

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def compute_cost(X, y, W, b):
    Z = compute_Z(X, W, b)
    A = sigmoid(Z)
    eps = 1e-15
    term1 = y * np.log(A + eps)
    term2 = (1 - y) * np.log(1 - A + eps)
    return -np.average(term1 + term2)

def compute_gradients(X, A, y):
    m = y.shape[1]
    dJdw = (1. / m) * ((A - y) @ X.T)
    dJdb = np.average(A - y)
    return (dJdw, dJdb)

def gradient_descent(X, y, W, b, iters, alpha, print_costs=False):
    costs = []
    for i in range(iters+1):
        # print('iter: ', i)
        if print_costs and (i % 2 == 0):
            costs.append([i, compute_cost(X, y, W, b)])
        Z = compute_Z(X, W, b)
        A = sigmoid(Z)
        (dw, db) = compute_gradients(X, A, y)
        # print('(dw, db: ', (dw, db))
        W = W - (alpha * dw)
        b = b - (alpha * db)
    return (W, b, costs)

def cost_vs_learning_rates(X, y, W, b, alphas, iters=400):
    costs = []
    for alpha in alphas:
        Wg, bg, Cg = gradient_descent(X, y, W, b, iters, alpha)
        calpha = compute_cost(X, y, Wg, bg)
        costs.append([alpha, calpha])
    return costs

def plot_cost_vs_learning(df):
  df = DataFrame(np.stack((ca_nda[:,0], ca_nda[:,1]), axis=1),
                 columns=['alpha', 'cost'])
  plot = (pn.ggplot(df, pn.aes('alpha', 'cost'))
          + pn.geom_point()
          + pn.geom_line())
  return plot

def predict(X, W, b):
    A = sigmoid(compute_Z(X, W, b))
    # A[A >= 0.5] = 1
    # A[A < 0.5] = 0
    A = np.where(A >= 0.5, 1, 0)
    return A

def accuracy(X, y, W, b):
    A = predict(X, W, b)
    correct = A == y
    return np.sum(correct) / y.shape[1]

def formatted_train_validate_accuracy(X, y, X_val, y_val, Wf, bf):
    train_accuracy = accuracy(X, y, Wf, bf)
    val_accuracy = accuracy(X_val, y_val, Wf, bf)
    return [('Train accuracy:', train_accuracy),
            ('Validation accuracy:', val_accuracy)]

from sklearn.linear_model import LogisticRegression
def sklearn_accuracy(X, y, X_val, y_val):
    lr = LogisticRegression(solver='liblinear')
    x = lr.fit(X.T, y.ravel())
    train_accuracy = lr.score(X.T, y.ravel())
    val_accuracy = lr.score(X_val.T, y_val.ravel())
    return [('Train accuracy:', train_accuracy),
            ('Validation accuracy:', val_accuracy)]
