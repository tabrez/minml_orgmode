
import numpy as np
from pandas import read_csv
import ipdb

class State:
    def init_with_random(self, unit_counts):
        np.random.seed(42)
        w = [np.random.rand(j, k) for j, k
             in zip(unit_counts[1:], unit_counts)]
        b = [np.random.rand(j, 1) for j
             in unit_counts[1:]]
        return (w, b)

    def init_with_zeros(self, unit_counts):
        w = [np.zeros(j, k) for j, k
             in zip(unit_counts[1:], unit_counts)]
        b = [np.zeros(j, 1) for j
             in unit_counts[1:]]
        return (w, b)

    def __init__(self, unit_counts, init_scheme='random', W=None, b=None):
        self.layer_count = len(unit_counts) - 1
        self.unit_counts = unit_counts
        if init_scheme == 'random':
            self.weights, self.biases = self.init_with_random(unit_counts)
        elif init_scheme == 'custom':
            self.weights, self.biases = W, b
        else:
            self.weights, self.biases = self.init_with_zeros(unit_counts)

def sigmoid(z):
    return 1. / (1 + np.exp(-z))

def sigmoid_prime(z):
    return sigmoid(z) * (1 - sigmoid(z))

def feed_forward(X, state, cache=False):
    a = X
    ac = []
    zc = []
    for wl, bl in zip(state.weights,
                      state.biases):
        z = np.dot(wl, a) + bl
        a = sigmoid(z)
        if cache:
            zc.append(z)
            ac.append(a)
    return (a, ac, zc)

def predict(X, s):
    A,_,_ = feed_forward(X, s, cache=False)
    return [1 if a > 0.5 else 0 for a in A[0]]

def compute_cost(A, y):
    return np.sum(np.abs(A - y))

def accuracy(X, y, s):
    A = predict(X, s)
    correct = A == y
    return np.sum(correct) / y.shape[1]

def compute_gradients(X, Y, A, Z, s):
    L = s.layer_count - 1
    W = s.weights
    dCdA = dCdZ = [np.zeros_like(a) for a in A]
    dCdW = [np.zeros_like(w) for w in W]
    dCdB = [np.zeros_like(b) for b in s.biases]
    dCdA[L] = [(-y / a) + ((1 - y) / (1 - a + 1e-15)) for (y, a) in zip(Y, A[L])]
    dCdZ[L] = dCdA[L] * sigmoid_prime(Z[L])
    for li in reversed(range(L+1)):
        if li != L:
            dCdZ[li] = (W[li+1].T @ dCdZ[li+1]) * sigmoid_prime(Z[li])
        dCdW[li] = dCdZ[li] @ (X.T if li ==0 else A[li-1].T)
        dCdB[li] = np.sum(dCdZ[li], axis=1, keepdims=True)
    return (dCdW, dCdB)

def gradient_descent(X, s, Y, k, epochs, alpha, cost_hist=False):
    m = X.shape[1]
    ch = []
    for epoch in range(epochs):
        for start in range(0, m, k):
            if cost_hist and epoch % 100 == 0:
                ch.append(compute_cost(predict(X, s), Y))
            end = start+k if start+k <= m else m
            mini_batch_X = X[:, start:end]
            mini_batch_Y = Y[:, start:end]
            _, A, Z = feed_forward(mini_batch_X, s, cache=True)
            delW, delB = compute_gradients(mini_batch_X,
                                           mini_batch_Y,
                                           A, Z, s)
            s.weights = [w - t2 for (w, t2) in zip(s.weights,
                                                   [alpha * delw for delw in delW])]
            s.biases = [b - t2 for (b, t2) in zip(s.biases,
                                                  [alpha * delb for delb in delB])]
    return (s.weights, s.biases, ch)

def try_toy_dataset():
    # X = np.array([[3], [1], [4], [2], [6], [5]])
    X = np.array([[3,1,2], [1,1,2], [4,1,2], [2,1,2], [6,1,2], [5,1,2]])
    y = np.array([[1, 0, 0]])
    s = State([6, 3, 1],
              init_scheme='custom',
              W=[np.array([[1, 2, 3, 4, 1, 2],
                           [1, 1, 1, 1, 1, 1],
                           [-1, -1, -1, -1, -1, -1]]),
                 np.array([[2, 3, 2]])],
              b=[np.array([[10], [11], [-10]]), np.array([4])])
    #_, A, Z = feed_forward(X, state, cache=True)
    s.weights, s.biases = gradient_descent(X,
                                           s,
                                           y,
                                           k=2,
                                           epochs=2,
                                           alpha=0.001)
    return feed_forward(X, state, cache=False)[0]

def load_dataset(path):
    df = read_csv(
        path,
        header=None,
        dtype=np.float32,
        na_values='?',
        usecols=[1,2,3,4,5,7,8,9,10]
    )
    return df.values

def try_bc_dataset():
    df = load_dataset('data/bc.csv')
    X = df[:, :-1].T
    y = df[np.newaxis, :, -1]
    s = State([X.shape[0], 3, 1])
    s.weights, s.biases, ch = gradient_descent(X, s, y,
                                               k=X.shape[1],
                                               epochs=1800,
                                               alpha=0.0015,
                                               cost_hist=True)
    return accuracy(X, y, s)
# return predict(X, s)

from logistic_regression.logreg import train_validate_split
def load_and_prep_data(path):
   df = read_csv(path)
   df = df.iloc[1:,:].values
   X = df[:, :-1].T
   y = df[:, -1]
   y = y.reshape(1, X.shape[1])

   X, X_val, y, y_val = train_validate_split(X, y, 0.85)
   return (X, y, X_val, y_val)

def try_bank_note_dataset():
    X, y, X_val, y_val = load_and_prep_data('data/bank_note_data.csv')
    s = State([X.shape[0], 3, 1])
    s.weights, s.biases, ch = gradient_descent(X, s, y,
                                               k=4, #int(X.shape[1]/8),
                                               epochs=80,
                                               alpha=0.02,
                                               cost_hist=False)
    print('training set accuracy: ', accuracy(X, y, s))
    return accuracy(X_val, y_val, s)

      # return predict(X, s)

if __name__ == '__main__':
      print(try_bank_note_dataset())
