#+SETUPFILE: ../theme-readtheorg.setup
#+TITLE:       Machine Learning from Scratch - Course 1
#+AUTHOR:      Tabrez Iqbal
#+Description: Overview and syllabus of first course on Machine Learning
#+LANGUAGE:    en
#+STARTUP: showall indent entitiespretty latexpreview
#+OPTIONS: ^:nil

* Python
- Official documentation
- Beginners: Effective Computation in Physics
- Effective Python: 59 Specific Ways to Write Better Python
- Tools: Jupyter Lab, VS Code, ipdb, pytest/doctests, github, conda

* Numpy
** TODO flesh out numpy/numpy_basics.py
** TODO add references for numpy
- Data Analysis Handbook
- Python for Data Analysis
- PyData videos
- Official documentation

* Plotting
** TODO Adapt chapter 3 from R for Data Science to Plotnine code
** TODO Introduction to Matplotlib

* Functions
- Functions as table, functions as graphs, functions using algebraic notation
- Single-variable and multi-variable functions(khan academy multi-var calc)
- Math vs. real world functions. Approximating functions to model something.
- Cartesian co-ordinates, line, plane, quadratic, exponential, log, polynomials
- Linear Regression: simple, multiple
- Independent, dependent variables
- Calculus Made Easy: Preliminary Chapter 1

* Calculus
- Differentiation
** TODO finish calculus/derivative_tutorial.py
** TODO Calculus Made Easy: Chapter II, IV, VI, IX, X, XI, XIV(e^x), XVI,
** TODO Caclulus Made Easy: Chapter XVII, XVIII, XIX
- 3Blue1Brown

* Statistics
- Sample, population, statistics, parameters, types of variables, distributions
- Measures of Central Tendencies
- Measures of Variability, standard deviation
- Normal & skewed distributions, z-score, standard error, p-value, normalization
- t-distribution, t-tests, t-value, confidence intervals
- statistical significance, effect size
- Co-variance, Pearson correlation, R-squared, F-statistic
- Hypothesis, types of errors
- ANOVA etc.

- Statistics in Plain English
- Think Stats
- Practical statistics for Data Scientist
- Statistics the easy way with R
- StatQuest

* Linear Algebra
- Pavel Grinfeld
- 3Blue1Brown

* Probability
- Chance project
- Think Bayes
- MOOC + Book by Tsitsiklis
- Blitzstein

* Numerical Computing
- Rachel's videos

* Linear Regression
- Dataset as a table. x1, x2, x3, features, y, labels.
- Dataset as a numpy ndarray. Dataset as a matrix.

* Gradient Descent
- loss/cost functions and their required properties for GD
  - differentiable
  - convex(?)
- explanation using derivatives
- plots using one input and one output variables
- contour & 3D plot using two input and one output variables
- linear algebra and multivariable gradient descent
- plots for learning rate, number of iterations

* Logistic Regression

* Activation functions
- why activation function
- Sigmoid, tanh
- ReLU, weak ReLU and their non-linearity

* Neural Networks

* PyTorch Example Project using Transfer Learning
