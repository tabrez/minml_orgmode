import sympy as sy

x, y, z = sy.symbols('x y z')
sy.init_printing(use_unicode=True)

sy.diff(5, x)
sy.diff(x, x)
sy.diff(x**2, x)
n = sy.symbols('n')
# general formula
sy.diff(x**n, x)
sy.diff(x**4, x)

# verify if the result makes sense
# At x = 13:
# 4 * (13 ** 3)
# 4x^3 = 4 x 13 x 13 x 13 = 8788
# ((13 + 0.001) ** 4 - (13 ** 4)) / 0.001 = 8789.01


def eval_for_x(x1):
    return 4 * (x1 ** 3)


def poor_mans_derivative(x1, delta):
    return ((x1 + delta) ** 4 - (x1 ** 4)) / delta
    (eval_for_x(13), poor_mans_derivative(13, 0.001))


# second derivative
sy.diff(x**4, x, 2)

# method 1
sy.diff(x**4 + 7 * x**2 - 3 * x + 4, x)
# method 2
expr = x**4 + 7 * x**2 - 3 * x + 4
expr.diff(x)
# method 3
diff_expr = sy.Derivative(x**4 + 7 * x**2 - 3 * x + 4, x)
diff_expr.doit()

# d(e^x) / dx
sy.diff(sy.exp(x), x)
diff_etox = sy.Derivative(sy.exp(x), x)
diff_etox
diff_etox.doit()
sy.diff(sy.exp(-x), x)
s = 1 / (1 + sy.exp(-z))
sy.diff(s, z)

# d(log(x)) / dx
diff_logx = sy.Derivative(sy.log(x), x)
diff_logx
diff_logx.doit()
# d(log(x+a)) / dx
a = sy.symbols('a')
diff_logxa = sy.Derivative(sy.log(x + a), x)
diff_logxa
diff_logxa.doit()

# Limits
sy.limit(s, z, 0)
sy.limit(s, z, sy.oo)
sy.limit(s, z, -sy.oo)

s.subs(z, 0)
s.subs(z, -10)
s.subs(z, 10)

a, y = sy.symbols('a y')
J = -1 * (y * sy.log(a) + (1 - y) * sy.log(1 - a))
sy.diff(J, a)
