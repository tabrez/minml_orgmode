def read_csv(file_path, type='int'):
    res = []
    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip('\n')
            elems = line.split(sep=',')
            res.append(elems)
    return res


read_csv('ex1data1.csv')
